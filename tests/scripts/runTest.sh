#!/bin/bash
# Run lvsTest.sh on single file

# Specify directory containing process scripts (currently the same as the directory containing runTests.sh)
SCRIPTDIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Run lvsTest on file , exiting with a return value of 1 when encountering a failure

file=$SCRIPTDIRECTORY/../verilog_src/$1

echo "Now processing $file"

$SCRIPTDIRECTORY/lvsTest.sh $file

if [[ $? -ne 0 ]]
then
    echo LVS test failed for $file
    exit 1;
else
    echo LVS test passed for $file
fi
