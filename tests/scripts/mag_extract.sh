#!/bin/bash
# Extract a spice file from a given magic file

magic -dnull -noconsole $1 << EOF
extract all
ext2spice scale off
ext2spice renumber off
ext2spice hierarchy on
ext2spice
quit -noprompt
EOF
