#!/bin/bash
# Install all of the programs in the ASIC tool flow into the directory specified by DESTDIR. 
# Run this in the directory containing the submodule directories

# ---------- SETUP ----------
# Directory variables
TOOLDIR=$(pwd)
DESTDIR=$(pwd)/tools
PATH=$DESTDIR/usr/local/bin:$DESTDIR/bin/:$PATH # This is exclusively for installing qflow

# Make destination directory
mkdir -p $DESTDIR

# ---------- INSTALLATION ----------

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install iverilog"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/iverilog
autoconf
./configure --prefix=$DESTDIR/
make
make check
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install yosys (and ABC as well)"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/yosys
make config-gcc
make
#make test
make install PREFIX="" DESTDIR=$DESTDIR

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install graywolf"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/graywolf
cmake -DCMAKE_INSTALL_PREFIX=$DESTDIR .
cmake .
make
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install qrouter"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/qrouter
./configure --with-tcl=/usr/lib/tcl8.5 --with-tk=/usr/lib/tk8.5 --prefix=$DESTDIR/ --with-libdir=$DESTDIR/share/qrouter
make
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install magic"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/magic
./configure --with-interpreter=tcl --with-tcl=/usr/lib/tcl8.5 --with-tk=/usr/lib/tk8.5 --prefix=$DESTDIR/
make
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install qflow"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/qflow
./configure --prefix=$DESTDIR --with-libdir=$DESTDIR/share/qflow --with-bindir=$DESTDIR/bin
make
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install netgen"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/netgen
./configure --with-tcl=/usr/lib/tcl8.5 --with-tk=/usr/lib/tk8.5 --prefix=$DESTDIR/
make
make install

echo ""
echo "--------------------------------------------------------------------------------"
echo "Build and Install irsim"
echo "--------------------------------------------------------------------------------"
echo ""
cd $TOOLDIR/irsim
./configure --prefix=$DESTDIR/
make
make install


# NOTE: The installation wiki says to copy osu035 standard cell magic views
# and ext circuit extractions to qflow tech dir. I believe that lvsTest.sh
# handles this in a temporary way, so it's not necessary for CI testing.
