#!/usr/bin/env python
# defAddRails.py - Adds vdd and gnd rails to a def file for LVS purposes only
# Copyright (C) 2016 Russell L Friesenhahn <russellf@utexas.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import sys
import argparse
import re

if __name__ == "__main__":

    parserp = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    subparsers = parserp.add_subparsers()
    parser = subparsers.add_parser('stop', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument(
        "-d",
        dest = "debug",
        default = False,
        action = "store_true",
        help = "Print out command-line arguments and commands to be created without actually doing anything"
        )

    parser.add_argument(
        'in_def',
        #nargs = '+',
        #dest = "in_def",
        action = "store",
        help = "input DEF file to have power rails added\n\n"
        )

    parser.add_argument(
        'out_def',
        #nargs = '+',
        #dest = "out_def",
        action = "store",
        help = "output DEF filename\n\n"
        )

    args = parser.parse_args()

    stdCell_m1width = 120
    pwrRail_m1width = 800
    pwrRail_dToCell = 1200
    gndRail_buffer  = 5000

    # DEF Sections can be in any order but qrouter writes them deterministically
    # Stage 0: Increase die area by 2um on both sides in x-direction
    # Stage 1: Find Components section
    # Stage 2: Process Components
    # Stage 3: Find Pins section and increase number by 2 (vdd and gnd)
    #          Add vdd and gnd to pins section
    # Stage 4: Find END DESIGN and insert a second SPECIAL NETS

    stage = 0


    # Need to save the entire list of components because they all connect to the
    # vdd and gnd nets#
    components = [] 
    rows = []
    topLtX = None
    topLtY = None
    topRtX = None
    topRtY = None
    vddPinX = None
    vddPinY = None
    gndPinX = None
    gndPinY = None
    compOrientation = None

    acceptedOrientations = ['N','S','FN','FS']

    with open(args.in_def, "r") as r, open(args.out_def, "w") as w:
        for line in r:

            if stage == 0:
                m = re.match("^DIEAREA\s+\(\s+(-?\d+\.?\d+)\s+(-?\d+\.?\d+)\s+\)\s+\(\s+(-?\d+\.?\d+)\s+(-?\d+\.?\d+)\s+\)\s+(;?)", line)
                if m is not None:
                    w.write("DIEAREA ( " + str(float(m.group(1)) - 400)
                                        + " " + m.group(2)
                                        + " ) ( "
                                        + str(float(m.group(3)) + 400)
                                        + " " + m.group(4)
                                        + " ) ;\n"
                                        )
                    #print("Found DIEAREA, inc stage")
                    #print("group 0 is " + m.group(0))
                    #print("group 1 is " + m.group(1))
                    #print("group 2 is " + m.group(2))
                    #print("group 3 is " + m.group(3))
                    #print("group 4 is " + m.group(4))
                    #print("group 5 is " + m.group(5))

                    stage += 1
                else:
                    w.write(line)

            elif stage == 1:
                m = re.match("^COMPONENTS", line)

                if m is not None:
                    stage += 1

                w.write(line)

            elif stage == 2:
                #m = re.match("-\s+([A-Z0-9_]+)\s+.+\(\s+(-?\d+\.?\d+)\s+(-?\d+\.?\d+)\s+\)\s+(\w+)\s+;", line)
                m = re.match("-\s+([A-Z0-9_]+)\s+.+\(\s+(-?\d+\.?\d+)\s+(-?\d+\.?\d+)\s+\)\s+(\w+)\s+;", line)

                if m is not None:
                    components.append(m.group(1))

                    if float(m.group(3)) not in rows:
                        rows.append(float(m.group(3)))

                    if ((float(m.group(2)) <= topLtX) and (float(m.group(3)) >= topLtY)) \
                        or (topLtX is None):
                        topLtX = float(m.group(2))
                        topLtY = float(m.group(3))
                        compOrientation = m.group(4)

                    if ((float(m.group(2)) >= topRtX) and (float(m.group(3)) >= topRtY)) \
                        or (topRtX is None):
                        topRtX = float(m.group(2))
                        topRtY = float(m.group(3))

                    #print("found component: " + m.group(1) + " | " + m.group(2) + " " + m.group(3))

                w.write(line)

                m = None
                m = re.match("^END COMPONENTS", line)

                if m is not None:
                    print("topLtXY is " + str(topLtX) + "," + str(topLtY))
                    print("Orientation is " + compOrientation)
                    print("topRtXY is " + str(topRtX) + "," + str(topRtY))
                    stage += 1
                    rows.sort(reverse=True,key=float)
                    print(rows)
                    print(len(rows))
                    print("End of COMPONENTS section")

                    if compOrientation not in acceptedOrientations:
                        print("Unsure what to do with orientation " + compOrientation + "\n")
                        sys.exit(-1)
                    else:
                        print("Component Orientation " + compOrientation + " recognized")

                        rows.insert(0,rows[0] + (abs(rows[0] - rows[1])))

                        m = re.match("\w?N", compOrientation)
                        if m is not None:
                            topRail = 'vdd'
                            vddIndexStart = 0
                            gndIndexStart = 1
                            print("vdd rails on top")
                        else:
                            topRail = 'gnd'
                            vddIndexStart = 1
                            gndIndexStart = 0
                            #rows.append(rows[-1] - (abs(rows[0] - rows[1])))
                            print("gnd rails on top")

                        print(rows)

                m = None

            elif stage == 3:

                m = re.match("^PINS\s+(\d+)", line)

                if m is not None:
                    w.write("PINS " + str((int(m.group(1)) + 2)) + " ;\n")
                    stage += 1
                    
                    vddPinX = topLtX - pwrRail_dToCell
                    vddPinY = rows[0] - ((rows[0] - rows[-1]) / 2)
                    gndPinX = topRtX + pwrRail_dToCell + gndRail_buffer
                    gndPinY = vddPinY
                    w.write("- vdd + NET vdd\n")
                    w.write("  + LAYER metal1 ( 0 0 ) ( 1 1 )\n")
                    w.write("  + PLACED ( " + str(vddPinX) + " " + str(vddPinY) + " ) N ;\n")

                    w.write("- gnd + NET gnd\n")
                    w.write("  + LAYER metal1 ( 0 0 ) ( 1 1 )\n")
                    w.write("  + PLACED ( " + str(gndPinX) + " " + str(gndPinY) + " ) N ;\n")
                else:
                    w.write(line)

                m = None

            elif stage == 4:
                m = re.match("^END DESIGN", line)

                if m is not None:
                    print(line)
                    for i in xrange(vddIndexStart, len(rows), 2): print(rows[i])

                    w.write("SPECIALNETS 2 ;\n")
                    w.write("- vdd\n")
                    w.write("  ( PIN vdd )\n")

                    for component in components:
                        w.write("  ( " + component + " vdd )\n")

                    print("vddIndexStart" + str(vddIndexStart))
                    w.write("+ ROUTED metal1 " \
                                + str(stdCell_m1width) + " ( " \
                                + str(topLtX) + " " \
                                + str(rows[vddIndexStart]) + " ) ( " \
                                + str(topLtX - pwrRail_dToCell - (stdCell_m1width / 2 )) + " " \
                                + "* " + ")\n")
                                #+ "* " + ")\n" \
                                #+ "  NEW metal1 " \
                                #+ str(stdCell_m1width) + " ( " \
                                #+ str(topLtX - pwrRail_dToCell) \
                                #+ " " \
                                #+ str(rows[vddIndexStart]) + " ) ( * "
                                #+ str(rows[vddIndexStart + 2]) + " ) \n")
                    
                    if len(rows) > 2 + vddIndexStart:
                        w.write("  NEW metal1 " \
                                + str(stdCell_m1width) + " ( " \
                                + str(topLtX - pwrRail_dToCell) \
                                + " " \
                                + str(rows[vddIndexStart]) + " ) ( * "
                                + str(rows[vddIndexStart + 2]) + " ) \n")
                        for i in xrange(vddIndexStart + 2, len(rows), 2):
                            #print("vdd" + str(i))
                            w.write("  NEW metal1 " \
                                    + str(stdCell_m1width) + " ( " \
                                    + str(topLtX) + " " \
                                    + str(rows[i]) + " ) ( " \
                                    + str(topLtX - pwrRail_dToCell - (stdCell_m1width / 2 )) + " " \
                                    + "* " + ")\n")

                            if (i + 2) <= len(rows) - 1:
                                #    print("vdd extra")
                                w.write("  NEW metal1 " \
                                    + str(stdCell_m1width) + " ( " \
                                    + str(topLtX - pwrRail_dToCell) \
                                    + " " \
                                    + str(rows[i]) + " ) ( * "
                                    + str(rows[i + 2]) + " )\n")

                    w.write(";\n")

                    w.write("- gnd\n")
                    w.write("  ( PIN gnd )\n")

                    for component in components:
                        w.write("  ( " + component + " gnd )\n")

                    print("gndIndexStart" + str(gndIndexStart))
                    w.write("+ ROUTED metal1 " \
                                + str(stdCell_m1width) + " ( " \
                                + str(topRtX) + " " \
                                + str(rows[gndIndexStart]) + " ) ( " \
                                + str(topRtX + gndRail_buffer + pwrRail_dToCell + (stdCell_m1width / 2 )) + " " \
                                + "* " + ")\n")
                                #+ "* " + ")\n" \
                                #+ "  NEW metal1 " \
                                #+ str(stdCell_m1width) + " ( " \
                                #+ str(topRtX + gndRail_buffer + pwrRail_dToCell) \
                                #+ " " \
                                #+ str(rows[gndIndexStart]) + " ) ( * "
                                #+ str(rows[gndIndexStart + 2]) + " ) \n")

                    if len(rows) > 2 + gndIndexStart:
                        w.write("  NEW metal1 " \
                                + str(stdCell_m1width) + " ( " \
                                + str(topRtX + gndRail_buffer + pwrRail_dToCell) \
                                + " " \
                                + str(rows[gndIndexStart]) + " ) ( * "
                                + str(rows[gndIndexStart + 2]) + " ) \n")
                    
                        for i in xrange(gndIndexStart + 2, len(rows), 2):
                            #print("gnd" + str(i))
                            w.write("  NEW metal1 " \
                                    + str(stdCell_m1width) + " ( " \
                                    + str(topRtX) + " " \
                                    + str(rows[i]) + " ) ( " \
                                    + str(topRtX + gndRail_buffer + pwrRail_dToCell + (stdCell_m1width / 2 )) + " " \
                                    + "* " + ")\n")

                            if (i + 2) <= len(rows) - 1:
                                #print("gnd extra")
                                w.write("  NEW metal1 " \
                                    + str(stdCell_m1width) + " ( " \
                                    + str(topRtX + gndRail_buffer + pwrRail_dToCell) \
                                    + " " \
                                    + str(rows[i]) + " ) ( * "
                                    + str(rows[i + 2]) + " )\n")


                    w.write(";\n")
                    w.write("END SPECIALNETS\n\n")

                w.write(line)
                m = None
            else:
                print("done")

            #print(line.rstrip())
            #w.write(line)

    r.close()
    w.close()
    print(args.in_def)
    print(args.out_def)
