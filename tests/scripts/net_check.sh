#!/bin/bash
# Run netgen to LVS check FILE

FILE=$1

netgen -noconsole << EOF
lvs layout/$FILE.spice {synthesis/$FILE.spc $FILE}
quit
EOF
