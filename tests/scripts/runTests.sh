#!/bin/bash
# Run lvsTest.sh on all files stored in verilog_src directory


TOOLDIR=$(pwd)

# Specify directory containing process scripts (currently the same as the
# directory containing runTests.sh)
SCRIPTDIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Specify the location of verilog files to test
#VFILES=$SCRIPTDIRECTORY/../verilog_src
VFILES="map9v3.v CIC.v CORDIC_serial.v fftbutterfly32.v"

# Run lvsTest on each file in VFILES, exiting with a return value of 1 when
# encountering a failure
#for f in $VFILES/*
for f in $VFILES
do
    file=$SCRIPTDIRECTORY/../verilog_src/$f
    echo "Now processing $file"
    $SCRIPTDIRECTORY/lvsTest.sh $file
    if [[ $? -ne 0 ]]
    then
        echo LVS test failed for $file
        exit 1;
    else
        echo LVS test passed for $file
    fi
done
