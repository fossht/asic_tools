#!/bin/bash
# Store layout views for the standard cells into a new "digital" directory

LIBPATH=$1/osu035/osu035_stdcells.gds2

mkdir digital
cp .magicrc digital
cd digital
magic -noconsole -dnull << EOF
gds read $LIBPATH
writeall force
quit
EOF
cd ..

