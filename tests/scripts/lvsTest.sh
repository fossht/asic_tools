#!/bin/bash
# LVS test verilog file passed as the parameter by following the instructions 
# laid out on www.opencircuitdesign.com/qflow/. 

# ========== VARIABLES ==========
# FILE.v to LVS test
FILEPATH=$1
FILEDIR=${FILEPATH%/*}
# Remove file path and suffix to aquire raw name
FILE=${FILEPATH##*/}
FILE=${FILE%.*}

# Specify tool installation location
TOOLDIRECTORY=$(pwd)/tools
PATH=$TOOLDIRECTORY/bin:$PATH

# Specify directory containing process scripts (currently the same as the directory containing lvsTest.sh)
SCRIPTDIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SRC=$SCRIPTDIRECTORY/../tech

# ========== SETUP ==========
# Setup project directory
DIR=test_designs/${FILE}
mkdir -p $DIR
mkdir $DIR/source $DIR/synthesis $DIR/layout

# Copy verilog source files
iverilog -M$FILEDIR/${FILE}.d $FILEPATH -y$FILEDIR
sed -i -e "s|.*\/\(.*\)\.v|cp \0 $DIR\/source\/\1.v|" $FILEDIR/${FILE}.d
. $FILEDIR/${FILE}.d

cp $SCRIPTDIRECTORY/../misc/project_vars.sh $DIR

# ========== EXECUTION ==========
# Execute qflow
cd ./$DIR
$TOOLDIRECTORY/bin/qflow synthesize place route $FILE 

# Connect Vdd and Gnd rails
cd layout
python $SCRIPTDIRECTORY/defAddRails.py ${FILE}.def ${FILE}_rails.def

# Create initial magic file
$SCRIPTDIRECTORY/load.sh $FILE $TOOLDIRECTORY

# Store Layout views
$SCRIPTDIRECTORY/stdcell_views.sh $SRC

# Extract spice file from ext file
$SCRIPTDIRECTORY/mag_extract.sh $FILE

# LVS check
cd ..
$SCRIPTDIRECTORY/net_check.sh $FILE > net_check.log

# Final result
grep "Result: Circuits match uniquely" net_check.log
