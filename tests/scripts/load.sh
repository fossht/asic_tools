#!/bin/bash
# Load and save a .mag file

FILE=$1
TOOLDIRECTORY=$2

# Create .mag file
magic -noconsole -dnull << EOF
lef read $TOOLDIRECTORY/share/qflow/tech/osu035/osu035_stdcells.lef
def read ${FILE}_rails.def
grid 1.6um 2.0um
writeall force $FILE 
quit -noprompt
EOF
