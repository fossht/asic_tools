#!/bin/tcsh
#------------------------------------------------------------
# project variables for project /home/rfriesen/builds/97fd2488/0/rfriesen/asic_tools/test_designs/CIC
#------------------------------------------------------------

# Synthesis command options:
# -------------------------------------------
# set yosys_options = 
# set yosys_script = 
# set yosys_nodebug = 
# set nobuffers = 
# set fanout_options = 

# Placement command options:
# -------------------------------------------
set initial_density = 0.5
set graywolf_options = "-n"

# Router command options:
# -------------------------------------------
# set route_layers = 
# set via_stacks = 
# set qrouter_options = 

# Minimum operating period of the clock (in ps)
# set vesta_options = --period 1E5

#------------------------------------------------------------

