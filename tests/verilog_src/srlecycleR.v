/*
 * srlecycle: Produces a high output every #(delay) cycles of en being high
 *            Initial condition is output high
 */

module srlecycleR(clk,q,en,rst);
  input clk,en,rst;
  output q;
  parameter delay=1;
  parameter INIT=17'h10000;

  wire [3:0] mydelay=(delay-2);

   wire      qo;
   
  SRL16ER Idelay(.D(q),.CLK(clk),.A0(mydelay[0]),.A1(mydelay[1]),.A2(mydelay[2]),.A3(mydelay[3]),.Q(qo),.CE(en),.RST(rst),.RSTVAL(INIT[15:0]));

  reg q;

  always @(posedge clk) begin
    if(rst)
      q<=(INIT>>16)&1;
    else if(en) 
      q<=qo;
  end

endmodule


