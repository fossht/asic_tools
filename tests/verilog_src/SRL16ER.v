module SRL16ER 
             (output Q,
             input A0, A1, A2, A3,
             input CE,
             input CLK,
             input D,
             input RST, 
             input [15:0] RSTVAL);

  reg [15:0] mem;
  
  assign Q=mem[{A3,A2,A1,A0}];

  always @(posedge CLK) begin
    if(RST)
      mem<=RSTVAL;
    else if(CE)
      mem<={mem[14:0],D};
  end

endmodule
 
