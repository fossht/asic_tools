module CIC(clk,rst,q,a,csync); // Cascaded Integrating-Comb filter
input clk;
input rst;
output signed [15:0] q;
input signed [15:0] a;
input csync;

reg csce;


/* Sign-extend input */

reg signed [32:0] i;

reg csync_delayed;


/********** Integrate ***********/

reg signed [32:0] iA0, iA1, iA2, iA3;	

always @(posedge clk) begin
   if(rst) begin
     iA0<=0;
     iA1<=0;
     iA2<=0;
     iA3<=0;
     i<=0;
   end else begin
     i<=$signed({a,1'b1});
     iA0<=iA0+i;
     iA1<=iA1+iA0;
     iA2<=iA2+iA1;
     iA3<=iA3+iA2;
   end
end


/************ Comb *************/

reg signed [32:0] csacc;
wire signed [32:0] csin=csync_delayed?iA3:csacc;
reg signed [32:0] cs1,cs2,cs3,cs4;

always @(posedge clk) begin 
  if (rst) begin
    csacc<=0;
    cs1<=0;
    cs2<=0;
    cs3<=0;
    cs4<=0;    
  end else if (csce) begin
    cs1<=csin;
    cs2<=cs1;
    cs3<=cs2;
    cs4<=cs3;

    csacc<=csin-cs4;
  end
end


wire signed [32:0] qq;

assign qq=csacc+65536;
assign q=qq[32:17];   


/********** Timing circuits ******/

srlecycleR #(4,17'h007) Icsce(clk,csce_or1,csync|csce_or1,rst);

always @(posedge clk) begin

  if(rst) begin
    csce<=0;
    csync_delayed<=0;  
  end else begin
    csce<=csync|csce_or1;
    csync_delayed<=csync;  
  end
end


endmodule





