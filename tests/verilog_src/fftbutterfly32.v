// Implements a pipelined, complex, radix-2 FFT butterfly
module fftbutterfly32(clk,Cr,Ci,Dr,Di,Tr,Ti,Ar,Ai,Br,Bi);
  input clk;
  output signed [31:0] Cr,Ci,Dr,Di;	// Outputs
  input signed [15:0] Ar,Ai,Br,Bi;	// Inputs
  input signed [15:0] Tr,Ti;		// Twiddle factors

  wire signed [31:0] Mr,Mi;  // intermediate value

  reg signed [31:0] Cr,Ci,Dr,Di;

  assign Mr=Br*Tr-Bi*Ti;  
  assign Mi=Br*Ti+Bi*Tr;

  always @(posedge clk) begin

    Cr<=Ar+Mr;    
    Ci<=Ai+Mi;    
    Dr<=Ar-Mr;    
    Di<=Ai-Mi;    
  end
endmodule
