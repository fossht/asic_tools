module hetero(clk,IN_I,IN_Q,LO_I,LO_Q,OUT_I,OUT_Q);
  input clk;
  input signed [15:0] LO_I,LO_Q,IN_I,IN_Q;
  output signed [31:0] OUT_I,OUT_Q;

  reg signed [31:0] P_II,P_QQ,P_IQ,P_QI; 
  reg signed [31:0] OUT_I,OUT_Q;

  always @(posedge clk) begin
    /* Products */

    P_II<=LO_I*IN_I;
    P_QQ<=LO_Q*IN_Q;
    P_IQ<=LO_I*IN_Q;
    P_QI<=LO_Q*IN_I;
 
    /* Sum */
    OUT_I<=P_II-P_QQ;
    OUT_Q<=P_IQ+P_QI;
  end 

endmodule

