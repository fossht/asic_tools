// Direct-form 3-tap FIR filter
module fir32(clk,z,x,h0,h1,h2);

input clk;
input signed [15:0] x, h0,h1,h2;
output signed [31:0] z;

reg signed [15:0] a,b;

reg signed [31:0] z;

always @(posedge clk) begin
  // Tapped-delay line
  a<=x;
  b<=a;

  // Compute output
  z<=x*h0+a*h1+b*h2;
end

endmodule
