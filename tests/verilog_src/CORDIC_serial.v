module CORDIC_serial(clk,rst,xo,yo,xi,yi,zi,ki);

input  clk;
input  rst;
output signed [15:0] xo,yo;
input  signed [15:0] xi,yi,zi,ki;


reg signed [15:0] x,y,z;

reg signed [14:0] r;

reg signed [31:0] xs,ys;

reg signed [15:0] zs;

always @(posedge clk) begin
  if(rst) begin
    r<=15'b010000000000000;
  end else begin
    r<={1'b0,r[0],r[13:1]};
    xs=r[13]?xi:((x*r)>>13);
    ys=r[13]?yi:((y*r)>>13);
    zs=r[13]?zi:z;

    if(r[13]) begin
      z<=zi;
      x<=xi;
      y<=yi;
    end else if(zs[15]) begin
      z<=zs+ki;
      x<=x+ys;
      y<=y-xs;
    end else begin
      z<=zs-ki;
      x<=x-ys;
      y<=y+xs;
    end
  end
end

  assign xo=x;
  assign yo=y;

endmodule
